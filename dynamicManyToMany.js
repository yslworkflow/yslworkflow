/*
button code is:

"  <a id='myButton' class=\"Vibrant \" style=\"width: 140px; text-align: center;\"  href=\"javascript:var args={manyToManyRecordColumn:3,manyToManyTable:'bpunbevkk',displayColumns:[6 ,7,10],filterColumns:[{id:6 },{id:7 }], manyToManyRelatedColumnToCurrentTable:6,manyToManyRelatedColumnToRefrenceTable:8,refranceTable:'bpuna8hhb',refrenceRecordIdColumn:3,refrenceTableRelatedToMany:8,conditionalColumnId:'',token:'bdgy9cmbrkjdi9cnhpc3scr93bef', title:'Manage students in this class',tab:''};var currentRecord="&[Record ID#]&";var conditionValue='' ; $.getScript(gReqAppDBID + '?a=dbpage&pagename=dynamicManyToMany.js');"&" void(0);\">Add/Update Class</a>"
*/

let filterColumns = args.filterColumns;

let displayColumns = args.displayColumns//[6 ,7];

var manyToManyTable = args.manyToManyTable //'bpunbevkk' ;
let manyToManyRecordColumn=args.manyToManyRecordColumn//3
let manyToManyRelatedColumnToCurrentTable=args.manyToManyRelatedColumnToCurrentTable//6;
let manyToManyRelatedColumnToRefrenceTable=args.manyToManyRelatedColumnToRefrenceTable//8;

let refranceTable = args.refranceTable//'bpuna8hhb';
let refrenceRecordIdColumn=args.refrenceRecordIdColumn//3;
let refrenceTableRelatedToMany = args.refrenceTableRelatedToMany//8;
//let conditionValue =args.conditionValue//"" ;
let conditionalColumnId =args.conditionalColumnId//"" ;



let token = args.token//'bdgy9cmbrkjdi9cnhpc3scr93bef';

let title=args.title//"Manage students in this class "

let tab=args.tab

//let currentTable=args.//'bpunaf6tp';
// currentRecord comes over
let clist=`${refrenceRecordIdColumn}`
//let uniqueId = Math.random().toString(36).substring(2) + Date.now().toString(36);
displayColumns.forEach((column,index) => {
 // if (index == 0) {

   // clist+=`${column.id}`
  //} else {
    clist += `.${column}`
  //}
})

var filters = '';
var num = 0;

getColumnNames(0)

function start() {


  var andBox = '<div id="andDiv"  style="margin-top:2px;padding-bottom:5px;display: none;"><select id="and" class="anyAllBox hybridStyle"><option value="all">all</option><option value="any">any</option></select> of these conditions are true</div>';
  var a = `<div data-row="${num}" style="padding-bottom: 5px; padding-top: 5px" id="filter${num}"><select name="critFID_${num}" id="critFID_${num}" onchange="critFIDchange(${num}, false, this)" onclick="critFIDclick(${num}, this)" class="hybridStyle" style="color: black; "> <option value="x5NULL" style="color:#707070;">Select a field...</option>`;
  filterColumns.forEach((col) => {
    a += `<option value="${col.id}">${col.name}</option>`;
  });
  a += `</select>`;
  var b = `<select name="how_${num}" id="how_${num}" onchange="howChange(this, ${num})" onfocus="critRowHasFocus(this,${num})" onblur="critRowLostFocus(this,${num})" class="hybridStyle" style=""><option value="CT">contains</option><option value="XCT">does not contain</option><option value="EX">is equal to</option><option value="XEX">is not equal to</option><option value="SW">starts with</option><option value="XSW">does not start with</option><option value="" disabled="">---------------</option><option value="LT">is less than</option><option value="LTE">is less than or equal</option><option value="GT">is greater than</option><option value="GTE">is greater than or equal</option></select>`;
  var c = `<input class="embedded-select-table hybridStyle" type="text" size="30" name="matchText_${num}" id="matchText_${num}" value="" style="margin-top: -3px;">`;
  var d = `<img class="insertCriterion criteriaButton " alt="ins" title="Insert after this" src="https://assets-cflare.quickbasecdn.net/res/20e2458-15/i/icons/16/icon-add.svg" style="opacity: 1;padding-left: 5px" onclick="addRow()">  
<span onclick="cancelFilter()" class="Icon Icon16 Delete" style="
    width: 8px !important;
    padding: 5px;
    background-repeat: no-repeat;
    padding-left: 10px;
    margin-left: 3px;
"></span>
</div>`;

  filters += andBox + a + b + c + d;

//put this back in,
//   query: `{${conditionalColumnId}.EX.${conditionValue}}`
  let ob = {
    apptoken: token,
    a: 'API_DoQuery',
    clist: clist
  }
  if (conditionValue != "") {
    ob.query = `{${conditionalColumnId}.EX.${conditionValue}}`
  }

  $.get(refranceTable, ob).then(function (xml) {
    var accounts = xml.getElementsByTagName('record');
    var tablePart1 = `<table width="100%" cellspacing="0" cellpadding="0" class="searchResults" style="border-width:0px;">
  <thead style="display:table-header-group">
  <tr class="hd FS-H3 Frozen">
  <td class="icr" style="padding-left: 20px;"><input type="checkbox" id="selectAll" class="deleteBox " onclick="selectAll()" >&nbsp;</td>`;
    let tablePart2 = ""

    //let colNum = 0;
    /* displayColumns.forEach((column) => {
       if (colNum == 0) {
         tablePart1 += '<td >';

       } else {

         tablePart1 += `<td align="right">`;
         colNum++;
       }
       tablePart1+=`<div class="hd-div"><span class="ColumnHeading">${column.name}</span></div></td>`
     })
   */


    var tablePart3 = `</tbody>
</table>
`;

    var query2 = '';

    for (i = 0; i < accounts.length; i++) {
      var account = accounts[i];
      tablePart2 += `<tr canedit="true" class="ev">
  <td class="icr" nowrap="" style="background-color: rgb(255, 255, 255); padding-left: 20px;"><input type="checkbox" class="deleteBox dialog_checkbox" data-accountnumber="${account.childNodes[1].innerHTML}" data-checked="false" >&nbsp;</td>
`
      for (let column = 2; column < account.childNodes.length - 2; column++) {
        if (account.childNodes[column].tagName == undefined) {
          continue;
        }
        if (i == 0) {
          if (column == 3) {
            tablePart1 += '<td >';

          } else {

            tablePart1 += `<td align="right">`;

          }
          tablePart1 += `<div class="hd-div"><span class="ColumnHeading">${makePresentable(account.childNodes[column].tagName)}</span></div></td>`

        }
        if (column == 3) {
          tablePart2 += `<td class="FirstColumn" style="background-color: rgb(255, 255, 255);">${account.childNodes[column].innerHTML}</td>`
        } else {
          tablePart2 += `<td align="right" class="NoWrap">${account.childNodes[column].innerHTML}</td>`
        }

      }
      /* var name = account.getElementsByTagName('account_name')[0].innerHTML;
       var address = account.getElementsByTagName('service_address')[0].innerHTML;
       var utility = account.getElementsByTagName('commodity_type')[0].innerHTML;
       var accountNumber = account.getElementsByTagName('account___pod_id')[0].innerHTML;
       tablePart2 += `<tr canedit="true" class="ev">
     <td class="icr" nowrap="" style="background-color: rgb(255, 255, 255); padding-left: 20px;"><input type="checkbox" class="deleteBox dialog_checkbox" data-accountnumber="${accountNumber}" data-checked="false" >&nbsp;</td>
   <td class="FirstColumn" style="background-color: rgb(255, 255, 255);">${name}</td>
   <td align="right" class="NoWrap">${address}</td>
   <td align="right" class="NoWrap">${utility}</td>
   </tr>`;*/
      tablePart2 += "</tr>"

    }
    tablePart1 += `
</tr>
</thead>
<tbody style="display:table-row-group">
  <tr class="HeaderBorder">
  <td colspan="${displayColumns.length + 1}" style="background-color: rgba(0, 0, 0, 0);">&nbsp;</td>
</tr>`;
    $('#dialog').dialog('destroy');
    $('#dialog').remove();
    $('body').prepend(`<div  id="dialog" title="${title}"><p>` + '<div id="filters">' + filters + '</div>' + tablePart1 + tablePart2 + tablePart3 + '</p></div>');

  $("selectAll").click(selectAll());
    $('#dialog').dialog({
      width: 800,
      buttons: [
        {
          text: 'Filter',
          click: function () {
            filter();
          }
        },
        {

          text: 'Save',
          click: function () {
            save();
            $(this).dialog('close');
          }
        }, {
          text: 'Cancel',
          click: function () {
            $(this).dialog('close');
          }
        }


      ], create: function () {
        $(this).css('maxHeight', 350);
      }
    });


    query2 = `{${manyToManyRelatedColumnToCurrentTable}.EX.${currentRecord}}`;
    $.get(manyToManyTable, {
      apptoken: token,
      a: 'API_DoQuery',
      query: query2,
      clist: `${manyToManyRelatedColumnToRefrenceTable}.${manyToManyRecordColumn}`
    }).then(function (xml2) {
      var accounts2 = xml2.getElementsByTagName('record');
      for (x = 0; x < accounts2.length; x++) {
        var account2 = accounts2[x];
        var checkedAccount = account2.childNodes[1].innerHTML;

        $.each($('.dialog_checkbox'), (index, box) => {
          // alert($(box).data("accountnumber"))

          if ($(box).data('accountnumber') == checkedAccount) {
            //alert("checked")
            $(box).attr('data-id', account2.childNodes[3].innerHTML);
            $(box).attr('data-checked', 'true');
            $(box).prop('checked', true);
          }
        });

      }


    });
  });
}
function save() {
  $.each($('.dialog_checkbox'), (index, box) => {
    if ($(box).data('checked') == false && $(box).prop('checked')) {
      $.get(manyToManyTable, {
        apptoken: token,
        a: 'API_AddRecord',
        ["_fid_"+manyToManyRelatedColumnToCurrentTable]: currentRecord,
        ["_fid_"+manyToManyRelatedColumnToRefrenceTable]: $(box).data('accountnumber')
      });


    }
    if ($(box).data('checked') == true && !($(box).prop('checked'))) {
      $.get(manyToManyTable, {
        apptoken: token,
        a: 'API_DeleteRecord',
        rid: $(box).data('id')
      });

    }

  });
  if(tab!=""&&tab!=undefined&&(!(window.location.href .endsWith('#'+tab)))){
    window.location.href = window.location.href+'#'+tab;

  }

   location.reload();
}


let deleted = [];

function filter() {
  let query = '';
  let condition = '';
  if ($('#and').val() == 'any') {
    condition = 'OR';
  }

  if ($('#and').val() == 'all') {
    condition = 'AND';
  }

  for (let x = 0; x < num + 1; x++) {

    let del = false;
    deleted.forEach((d) => {
      if (d == x) {
        del = true;
      }
    });
    if (del == false) {
      if (x != 0) {
        query += `${condition}`;
      }
      var field = $(`#critFID_${x}`).val();
      var how = $(`#how_${x}`).val();
      var text = $(`#matchText_${x}`).val();
      if(conditionValue!="") {
        query += `({${conditionalColumnId}.EX.${conditionValue}}AND{${field}.${how}.${text}})`;
      }else{
        query += `{${field}.${how}.${text}}`
      }

    }

  }


  $.get(refranceTable, {
    apptoken: token,
    a: 'API_DoQuery',
    query: query,
    clist:refrenceTableRelatedToMany
  }).then(function (xml) {
    $.each($('.ev'), (i, e) => {
      $(e).hide();
    });
    var accounts = xml.getElementsByTagName('record');
    for (i = 0; i < accounts.length; i++) {
      var account = accounts[i];
          var accountNumber = account.childNodes[1].innerHTML;

      // alert($(box).data("accountnumber"))
      $.each($('.ev'), (i, e) => {
        if ($(e).find('.dialog_checkbox').data('accountnumber') == accountNumber) {
          $(e).show();

        }
      });


    }

  });

}


function addRow() {
  $('#andDiv').show();
  num++;
  var a = `<div class="extraFilter" data-row="${num}" style="padding-bottom: 5px; padding-top: 5px" id="filter${num}"><select name="critFID_${num}" id="critFID_${num}" onchange="critFIDchange(${num}, false, this)" onclick="critFIDclick(${num}, this)" class="hybridStyle" style="color: black; "> <option value="x5NULL" style="color:#707070;">Select a field...</option>`;
  filterColumns.forEach((col) => {
    a += `<option value="${col.id}">${col.name}</option>`;
  });
  a += `</select>`;
  var b = `<select name="how_${num}" id="how_${num}" onchange="howChange(this, ${num})" onfocus="critRowHasFocus(this,${num})" onblur="critRowLostFocus(this,${num})" class="hybridStyle hi" style=""><option value="CT">contains</option><option value="XCT">does not contain</option><option value="EX">is equal to</option><option value="XEX">is not equal to</option><option value="SW">starts with</option><option value="XSW">does not start with</option><option value="" disabled="">---------------</option><option value="LT">is less than</option><option value="LTE">is less than or equal</option><option value="GT">is greater than</option><option value="GTE">is greater than or equal</option></select>`;
  var c = `<input class="embedded-select-table hybridStyle" type="text" size="30" name="matchText_${num}" id="matchText_${num}" value="" style="margin-top: -3px;">`;
  var d = `<img class="insertCriterion criteriaButton " alt="ins" title="Insert after this" src="https://assets-cflare.quickbasecdn.net/res/20e2458-15/i/icons/16/icon-add.svg" style="opacity: 1;padding-left: 5px" onclick="addRow()">`;
  var e = `<img id="minus${num}" class="deleteCriterion criteriaButton " alt="del" title="Delete this" src="https://assets-cflare.quickbasecdn.net/res/20e2458-15/i/icons/16/icon-remove.svg" style="opacity: 1;padding-left: 5px" onclick="deleteRow(${num})"></div>`;
  $('#filters').append(a + b + c + d + e);

}

function deleteRow(r) {
  if ($('.hi').length == 1) {
    $('#andDiv').hide();
  }
  $(`#filter${r}`).remove();
  deleted.push(r);
}

function selectAll() {
  if ($('#selectAll').prop('checked')) {
    $.each($('.dialog_checkbox'), (index, box) => {
      if ($(box).is(':visible')) {
        $(box).prop('checked', true);

      }

    });

  } else {
    $.each($('.dialog_checkbox'), (index, box) => {
      if ($(box).is(':visible')) {
        $(box).prop('checked', false);

      }


    });

  }
}
function makePresentable(s){
  let st =s.replace(/_/g," ");
  return st.replace(/\w\S*/g, function(txt){
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });

}
function getColumnNames(c) {
  $.get(refranceTable, {
    apptoken: token,
    a: 'API_GetFieldProperties',
    fid: filterColumns[c].id }).then(function (xml) {
    filterColumns[c].name=xml.getElementsByTagName("label")[0].innerHTML

    if(c+1<filterColumns.length){
         getColumnNames(c+1)
      }else{
      start();
    }
  })

}
function cancelFilter() {
  $.each($('.extraFilter'),(i,e)=>{$(e).remove()})
  $.each($('.ev'), (i, e) => {

    $(e).show()
  })
  $(`#matchText_0`).val(" ")

}

//?a=er&rid=37&rl=s4k#tab_t8
/*

window.location.href = window.location.href+'#tab_t8';
if(!(window.location.href.endsWith('#tab_t8'))){
  window.location.href = window.location.href+'#tab_t8';
  location.reload()
}
*/
